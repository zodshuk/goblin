;; gob.asm - the gob assembly (aka. gasm) standard library
%define CONSOLE 0
%define EOS 68

%var char 0

;; functions are called by putting a pointer to the first
;; argument in rZ
print:
	ldy EOS
loop:
	out CONSOLE
	inc rz
	ldx$* rz

	mov char	;; move value in z into memory
			;; so it doesn't get overwritten
			;; by `equ`

	equ
	jif .end

	ldz* char

	jmp .loop
end:	
	ret
