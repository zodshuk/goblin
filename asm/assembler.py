#!/usr/local/bin/python3
import sys
import enum

class Map(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__

class Token(Map): pass

class TokenType(enum.Enum):
    OPCODE,       \
    NUMBER_HEX,   \
    NUMBER_DEC,   \
    STRING,       \
    CHAR,         \
    REGISTER,     \
    DIRECTIVE,    \
    VAR_REF,      \
    LABEL_REF,    \
    LABEL =       \
        range(10)

# Unlike special forms the value of literals
# is not modified during assembling
TokenTypeLITERALS = [
    TokenType.OPCODE,
    TokenType.NUMBER_HEX,
    TokenType.NUMBER_DEC,
    TokenType.CHAR,
    TokenType.REGISTER
]

OPCODES = [
    "hlt",
    "ldx",
    "ldx*",
    "ldx$*",
    "ldy",
    "ldy*",
    "ldz",
    "ldz*",
    "mov",
    "add",
    "equ",
    "jmp",
    "jif",
    "cll",
    "ret",
    "inc",
    "out"
]

REGISTERS = [
    "pc",
    "fl",
    "io",
    "x",
    "y",
    "z"
]

CHARSET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ .,?!§"
STRING_TERMINATOR = 68

module_dir = "."

def error(msg): print(f"Error: {msg}"); sys.exit(1)

def tokenize(program: str) -> [str]:
    _tokens = []
    current_token = ""

    state = Map({
        "inString": False,
        "inComment": False
    })

    peek = lambda a, i: None if i >= len(a) else a[i]

    for c, char in enumerate(program + '\0'):
        match char:
            case '\n' if state.inComment:
                state.inComment = False
            case _ if state.inComment:
                continue
            case '"':
                if state.inString:
                    current_token += '"'
                    _tokens.append(current_token)
                    current_token = ""
                    state.inString = False
                else:
                    state.inString = True
                    current_token += '"'
            case _ if state.inString:
                current_token += char
            case ";" if peek(program, c + 1) == ";":
                state.inComment = True
            case ' ' | '\n' | '\0' | '\t':
                if current_token:
                    _tokens.append(current_token)
                    current_token = ""
            case _:
                current_token += char

    return _tokens

def parseToken(token: str) -> Token:
    isDecimal = lambda t: sum([int(c.isdigit()) for c in t]) == len(t)

    if token in OPCODES:
        type_ = TokenType.OPCODE
        value = OPCODES.index(token)
    elif token[0] == '\'':
        type_ = TokenType.CHAR
        value = CHARSET.index(token[1])
    elif token[0] == '"':
        type_ = TokenType.STRING
        value = [CHARSET.index(c) for c in token[1:-1]] + \
            [STRING_TERMINATOR]
    elif token[0] == '.':
        type_ = TokenType.LABEL_REF
        value = token[1:]
    elif token[0] == '%':
        type_ = TokenType.DIRECTIVE
        value = token[1:]
    elif token[0] == "r" and 1 < len(token) < 4:
        type_ = TokenType.REGISTER
        value = REGISTERS.index(token[1:])
    elif token[-1] == ':':
        type_ = TokenType.LABEL
        value = token[:-1]
    elif token[0:2] == '0x':
        type_ = TokenType.NUMBER_HEX
        value = int(token, 16)
    elif isDecimal(token):
        type_ = TokenType.NUMBER_DEC
        value = int(token)
    else:
        type_ = TokenType.VAR_REF
        value = token

    return Token({
        "type": type_,
        "value": value
    })

def parse(tokens: [str]) -> [Token]:
    tree = []

    for token in tokens:
        tree.append(parseToken(token))

    return tree

def assemble(syntaxTree: [Token]) -> [int]:
    bytecode = []
    data = []

    consts = {}
    vars_ = []
    labels = []

    state = Map({
        "inDirective": False
    })

    directive = None

    tokenSize = lambda t: len(t) if isinstance(t, str) else 1
    
    for token in syntaxTree:
        match token.type:
            case TokenType.DIRECTIVE:
                state.inDirective = True
                directive = Map({
                    "operator": token.value,
                    "name": None,
                    "value": None
                })

            case TokenType.VAR_REF if state.inDirective:
                directive.name = token.value

                if directive.operator == "import":
                    module_path = f"{module_dir}/{directive.name}.asm"
                    with open(module_path, "r") as f:
                        module = f.read()
                    syntaxTree += parse(tokenize(module))
                
            case _ if state.inDirective:
                directive.value = token.value

                match directive.operator:
                    case "define":
                        consts[directive.name] = directive.value
                    case "var":
                        vars_.append(Map({
                            "name": directive.name,
                            "size": tokenSize(token),
                            "initValue": token.value,
                            "dataOffset": len(data),
                            "address": None
                        }))
                        data += token.value \
                            if isinstance(token.value, list) \
                            else [token.value]
                    case _:
                        error("Unknown directive " + directive.operator)
                
                state.inDirective = False
                
            case _ if token.type in TokenTypeLITERALS:
                bytecode.append(token.value)
                
            case TokenType.VAR_REF:
                if token.value in consts:
                    bytecode.append(consts[token.value])
                else:
                    bytecode.append(token)

            case TokenType.LABEL_REF:
                bytecode.append(token)
                
            case TokenType.LABEL:
                labels.append(Map({
                    "name": token.value,
                    "address": None,
                }))
                bytecode.append(token)

    # Calculate label addresses
    findLabelIndex = lambda lName: [l.name for l in labels].index(lName)
    findLabel = lambda lName: labels[findLabelIndex(lName)]

    for b, byte in enumerate(bytecode):
        if isinstance(byte, Token):
            if byte.type == TokenType.LABEL:
                labelIndex = findLabelIndex(byte.value)
                labels[labelIndex].address = b
                del bytecode[b]
            
    # Calculate variable addresses
    program_size = len(bytecode)

    for var in vars_:
        var.address = var.dataOffset + program_size

    # Substitute references
    findVar = lambda vName: vars_[[v.name for v in vars_].index(vName)]

    for b, byte in enumerate(bytecode):
        if isinstance(byte, Token):
            match byte.type:
                case TokenType.VAR_REF:
                    bytecode[b] = findVar(byte.value).address
                case TokenType.LABEL_REF:
                    bytecode[b] = findLabel(byte.value).address

    return bytecode + data

if __name__ == "__main__":
    if len(sys.argv) < 2:
        error("Usage: python assembler.py <input_file> <*output_file>")

    input_file = sys.argv[1]

    output_file = input_file.split(".")[0] + ".bin" \
        if len(sys.argv) < 3 else sys.argv[2]

    if '-m' in sys.argv:
        module_dir = sys.argv[sys.argv.index('-m') + 1]

    with open(input_file, "r") as f:
        program = f.read()

    bytecode = assemble(
        tree := parse(
            tokens := tokenize(
                program
            )
        )
    )

    with open(output_file, "wb") as f:
        f.write(bytearray(bytecode))

    print(
        f"(gasm) Successfully assembled {input_file} -> " + \
        f"{output_file} [{len(bytecode)} Bytes]"
    )
  
