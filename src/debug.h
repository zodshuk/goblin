#include <string.h>

#include "goblin.h"

#define MAX_INPUT 100
#define MAX_TOKENS 5

Gw_t inspectMemory(Goblin* gob, char* addr, Gw_t hexMode)
{
  long int address;
  char *endptr;
  address = strtol(addr, &endptr, 0);

  if (endptr == addr || *endptr != '\0') {
    printf("Command execution error: invalid address %s\n", addr);
    return 1;
  } else if (address < 0 || address >= MEM_SIZE) {
    printf("Command execution error: address %s out of range\n", addr);
    return 1;
  }

  printf("%ld: ", address);

  for (int i = 0; i < 8; i++)
    printf(hexMode ? "%02x " : "%03d ", gob->memory[address + i]);

  printf("\n");
  return 0;
}

Gw_t inspectRegisters(Goblin* gob)
{
  printf("Reg FL: %x\n", gob->reg[FL]);
  printf("Reg IO: %x\n", gob->reg[IO]);
  printf("Reg X: %x\n", gob->reg[rX]);
  printf("Reg Y: %x\n", gob->reg[rY]);
  printf("Reg Z: %x\n", gob->reg[rZ]);
  return 0;
}

Gw_t debugMode(Goblin* gob, Gw_t* debug)
{
  char input[MAX_INPUT];
  char *tokens[MAX_TOKENS];
  char *token;
  int num_tokens;

  Gw_t hexMode;
  
  printf("<goblin:debug> ");
  fgets(input, sizeof(input), stdin);
  input[strlen(input) - 1] = '\0';

  token = strtok(input, " ");
  num_tokens = 0;

  while (token != NULL && num_tokens < MAX_TOKENS)
  {
    tokens[num_tokens++] = token;
    token = strtok(NULL, " ");
  }

  if (num_tokens > 0) {
    // Halt execution
    if (strcmp(tokens[0], "kill") == 0) {
      return 1;
    // Exit debug mode
    } else if (strcmp(tokens[0], "ex") == 0) {
      *debug = off;
    // Execute next instruction
    } else if (strcmp(tokens[0], "step") == 0) {
      printf("[goblin:debug pc:%d *stepping*]\n", gob->reg[PC]);
      gobExecute(gob);
    // Inspect memory
    } else if (strcmp(tokens[0], "im") == 0) {
      hexMode = off;
      if (num_tokens > 2)
        if (strcmp(tokens[2], "-h") == 0) hexMode = on;
      inspectMemory(gob, tokens[1], hexMode);
    // Inspect registers
    } else if (strcmp(tokens[0], "ir") == 0) {
      inspectRegisters(gob);
    } else {
      printf("Error: unknown command %s\n", tokens[0]);
    }
  }

  return 0;
}
