#ifndef GOBLIN_H
#define GOBLIN_H
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define on 1
#define off 0

#define MEM_SIZE 256
#define PSTACK_SIZE 32
#define NUM_DEVICES 1

typedef uint8_t Gw_t;

enum {
  PC = 0,
  FL,
  IO,
  rX,
  rY,
  rZ,
  rA,
  rB,
  rC,
  NUM_REGISTERS
};  // Registers

enum {
  fHLT = 0,
  fX,
};  // Flags

enum {
  HLT = 0,
  LDX,
  LDXa,
  LDXn,
  LDY,
  LDYa,
  LDZ,
  LDZa,
  MOV,
  ADD,
  EQU,
  JMP,
  JIF,
  CLL,
  RET,
  INC,
  OUT,
  NUM_INSTRUCTIONS
};  // Instruction Set

typedef struct Goblin Goblin;
typedef void (*Instruction)(Goblin*);
typedef void (*Device)(Gw_t);

const char CHARSET[] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ .,?!\n";

typedef struct Goblin
{
  Gw_t reg[16];
  Gw_t memory[MEM_SIZE];
  Gw_t pStack[PSTACK_SIZE];
} Goblin;

Goblin* makeGoblin(void)
{
  Goblin* goblin = malloc(sizeof(Goblin));

  for (int m = 0; m < MEM_SIZE; m++)
    goblin->memory[m] = 0;

  for (int r = 0; r < NUM_REGISTERS; r++)
    goblin->reg[r] = 0;

  return goblin;
}

Gw_t gobGobble(Goblin* gob)
{ return gob->memory[gob->reg[PC]++]; }

Gw_t gobCheckFlag(Goblin* gob, Gw_t flag)
{ return (gob->reg[FL] & (1 << flag)) != off; }

void gobSetFlag(Goblin* gob, Gw_t flag, Gw_t status)
{
  if (status)
    gob->reg[FL] |= (1 << flag);
  else
    gob->reg[FL] &= ~(1 << flag);
}

void iLDX(Goblin* gob)
{ gob->reg[rX] = gobGobble(gob); }

void iLDXa(Goblin* gob)
{ gob->reg[rX] = gob->memory[gobGobble(gob)]; }

void iLDXn(Goblin* gob)
{ gob->reg[rX] = gob->memory[gob->reg[gobGobble(gob)]]; }

void iLDY(Goblin* gob)
{ gob->reg[rY] = gobGobble(gob); }

void iLDYa(Goblin* gob)
{ gob->reg[rY] = gob->memory[gobGobble(gob)]; }

void iLDZ(Goblin* gob)
{ gob->reg[rZ] = gobGobble(gob); }

void iLDZa(Goblin* gob)
{ gob->reg[rZ] = gob->memory[gobGobble(gob)]; }

void iMOV(Goblin* gob)
{ gob->memory[gobGobble(gob)] = gob->reg[rZ]; }

void iADD(Goblin* gob)
{ gob->reg[rZ] = gob->reg[rX] + gob->reg[rY];}

void iEQU(Goblin* gob)
{ gob->reg[rZ] = gob->reg[rX] == gob->reg[rY]; }

void iJMP(Goblin* gob)
{ gob->reg[PC] = gobGobble(gob); }

void iJIF(Goblin* gob)
{
  Gw_t addr = gobGobble(gob);
  if (gob->reg[rZ] == 1)
    gob->reg[PC] = addr;
}

void iCLL(Goblin* gob)
{
  Gw_t addr = gobGobble(gob);
  gob->pStack[++gob->reg[rC]] = gob->reg[PC];
  gob->reg[PC] = addr;
}

void iRET(Goblin* gob)
{
  gob->reg[PC] = gob->pStack[gob->reg[rC]--];
}

void iINC(Goblin* gob)
{ gob->reg[gobGobble(gob)]++; }

void iOUT(Goblin* gob)
{ gob->reg[IO] |= (1 << gobGobble(gob)); }

void iHLT(Goblin* gob)
{ gobSetFlag(gob, fHLT, on); }

Instruction INSTRUCTIONS[NUM_INSTRUCTIONS] = {
  iHLT,
  iLDX,
  iLDXa,
  iLDXn,
  iLDY,
  iLDYa,
  iLDZ,
  iLDZa,
  iMOV,
  iADD,
  iEQU,
  iJMP,
  iJIF,
  iCLL,
  iRET,
  iINC,
  iOUT
};

void Console(Gw_t data)
{
  printf("%c", CHARSET[data]);
}

Device Devices[NUM_DEVICES] = {
  Console,
};

void gobExecute(Goblin* gob)
{
  Gw_t nextInstruction = gobGobble(gob);
  (INSTRUCTIONS[nextInstruction])(gob);

  for (int d = 0; d < NUM_DEVICES; d++) {
    if (gob->reg[IO] & (1 << d)) {
      (Devices[d])(
        gob->memory[gob->reg[rZ]]
      );
      gob->reg[IO] &= ~(1 << d);
    } 
  }
}
#endif // GOBLIN_H
