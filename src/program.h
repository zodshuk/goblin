#include <string.h>

#include "goblin.h"

typedef struct {
  Gw_t bytes[MEM_SIZE];
  size_t size;
  char errorMessage[100];
} program_t;

void usage() {
  fprintf(stderr, "Usage: goblin <path to program> -[d]\n");
}

program_t readProgram(const char* path)
{
  program_t prog = {"", -1, ""};
  FILE *file = fopen(path, "rb");
  if (file == NULL) {
      strcpy(prog.errorMessage, "Error opening file");
      return prog;
  }

  prog.size = fread(prog.bytes, sizeof(Gw_t), MEM_SIZE, file);
  fclose(file);

  return prog;
}

void gobLoadProgram(Goblin* gob, program_t program)
{
  for (size_t i = 0; i < program.size; i++)
    gob->memory[i] = program.bytes[i];
}
