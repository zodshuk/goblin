#include <stdio.h>
#include <string.h>

#include "goblin.h"
#include "program.h"
#include "debug.h"

int main(int argc, char *argv[])
{
  Gw_t debug = off;
  
  if (argc < 2) {
    usage();
    return 1;
  }
  
  for (int i = 2; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
        case 'd':
          debug = on;
          break;

        default:
          fprintf(stderr, "Unknown option -%c\n", argv[i][1]);
          return 1;
      }
    }
  }
  
  program_t program = readProgram(argv[1]);

  if (program.size == -1) {
    perror(program.errorMessage);
    return 1;
  }

  Goblin* gob = makeGoblin();

  gobLoadProgram(gob, program);

  while (gobCheckFlag(gob, fHLT) != on)
  {
    if (debug) {
      if (debugMode(gob, &debug)) return 0;
    } else {
      gobExecute(gob);
    }
  }

  return 0;
}
