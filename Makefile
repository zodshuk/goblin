.PHONY: vm run debug gasm cleanup

# make vm
# make gasm asm=test.asm
# make run rom=test.bin

VM = ./src/main.c
EXECUTABLE = ./build/goblin
ASSEMBLER = ./asm/assembler.py
MODULES = ./asm

COMPILER = cc
CFLAGS = -o $(EXECUTABLE)

vm: $(VM)
	$(COMPILER) $(VM) $(CFLAGS)

run: $(EXECUTABLE)
	$(EXECUTABLE) $(rom)

debug: $(EXECUTABLE)
	$(EXECUTABLE) $(rom) -d

gasm: $(ASSEMBLER)
	$(ASSEMBLER) $(asm) -m $(MODULES)

cleanup:
	rm -rf __pycache__/
	-rm -f ./src/*~
	-rm -f ./asm/*~
	-rm -f ./*~
	-rm -f ./asm/*.bin
