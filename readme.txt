+---------------------------------------------------------+
|                THE GOBLIN VIRTUAL MACHINE               |
|                ##########################               |
|                                                         |
| THE GOBLIN VM IS A 16 BIT VIRTUAL MACHINE. INSTRUCTIONS |
| ARE 8 BITS,  BUT OPTIONALLY MAY SUBSUME UP TO TWO BYTES |
| AS OPERANDS, ALLOWING FOR 16 BIT ADDRESSING.            |
| 							  |
| REGISTERS:						  |
|  (*1) rX and rY are used as arguments to arithmetic op- |
|       erations.	    	    	      	 	  |
|  (*2) rZ is the accumulator register; itholds the resu- |
|       lt of arithmetic operations, and it is used as an |
|       argument to MOV. 		       	     	  |
| 	    0000 - PC -	PROGRAM COUNTER			  |
| 	    0001 - FL -	FLAGS				  |
| 	    0010 - IO -	IO PORTS			  |
| 	    0011 - rX -	GENERAL PURPOSE REGISTER X *1	  |
| 	    0100 - rY -	GENERAL PURPOSE REGISTER Y 	  |
| 	    0101 - rZ -	GENERAL PURPOSE REGISTER Z *2	  |
| 	    0110 - rA -	GENERAL PURPOSE REGISTER A 	  |
| 	    0111 - rB -	GENERAL PURPOSE REGISTER B	  |
| 	    	      				 	  |
| INSTRUCTION MODES:					  |
| 	    0000 - I   (   ) -  IMMEDIATE		  |
| 	    0001 - R   ($  ) -  REGISTER		  |
| 	    0010 - A   (*  ) -  IMMEDIATE ADDR		  |
| 	    0011 - N   ($* ) -  REGISTER ADDR		  |
| 	    0100 - P   (** ) -  MEMORY ADDR		  |
| 	    0101 - A16 (*+ ) -  (16-Bit) IMMEDIATE ADDR	  |
| 	    0111 - P16 (**+) -  (16-Bit) MEMORY ADDR	  |
| 							  |
| INSTRUCTION SET:					  |
| 	    0x0000 - HLT -	SET HALT FLAG.		  |
| 	    0x0001 - LDX -	LOAD INTO X REGISTER.	  |
| 	    ...						  |
| 	    0x0008 - LDY -	SEE LDX.		  |
| 	    ...						  |
| 	    0x000f - LDZ -	SEE LDX.		  |
| 	    ...						  |
| 	    0x0016 - LDA -	SEE LDX.		  |
| 	    ...						  |
| 	    0x001d - LDB -	SEE LDX.		  |
| 	    ...						  |
| 	    0x0024 - ADD -	rZ = rX + rY		  |
| 	    0x0025 - SUB -	rz = rX - rY		  |
| 	    0x0026 - MUL -	rz = rX * rY		  |
| 	    0x0027 - DIV -	rz = rX / rY		  |
| 	    0x0028 - XOR -	rz = rX XOR rY		  |
| 	    0x0029 - NOT -	rz = NOT rX		  |
| 	    0x002a - AND -	rz = rX AND rY		  |
| 	    0x002b - LOR -	rz = rX OR rY		  |
|	    0x002c - EQU -	rz = rX == rY		  |
|	    0x002d - LST -	rz = rX < rY		  |
|	    0x002e - GRT -	rz = rX > rY		  |
|	    0x002f - EQZ -	rz = rX == 0		  |
|	    0x002f - NZR -	rz = rX != 0		  |
| 	    0x0030 - JMP -	PC = A/N/P/A16/P16	  |
| 	    ...	     	 	     			  |
| 	    0x0035 - JIF -	rZ == 1 ? JUMP		  |
| 	    ...	     	 	      	  		  |
| 	    0x003a - CLL -	CALL A/N/P/A16/P16	  |
| 	    ...						  |
| 	    0x003f - CIF -	rZ == 1 ? CALL		  |
| 	    ...	     	 	      	  		  |
| 	    0x0044 - RET -	RETURN			  |
| 	    0x0045 - INT -	SET FLAG BY IM. BITMASK	  |
| 	    0x0046 - CLR -	CLEAR INT AKA UNSET FLG   |
| 	    0x0047 - INC - 	r = r + 1     	    	  |
| 	    0x0048 - DEC -	r = r - 1		  |
| 	    0x0049 - OUT -	SET PORT (rIO) BITMASK	  |
| 	    0x004a - MOV - 	A/N/P/A16/P16 = rZ	  |
| 	    ...	     	 		      		  |
| 	    0x007f - NOP -	RESERVED		  |
|							  |
| Devices:						  |
| 	The GVM has 8 16-bit registers, including rIO wh- |
|	ich represents DEVICES as an IN and OUT bit, thus |
|	allowing for 8 DEVICES.   	      	  	  |
| 							  |
| 	In order to output to a device,  write a 1 to the |
|	corresponding output bit for that device. (ie. by |
|	using OUT) DEVICES expect an address in rZ point- |
| 	ing to some data in memory to be output.	  |
|							  |
| 	To recieve input from a device set the IN bit for |
| 	that device.  DEVICES expect an address in memory |
| 	to write any data to, a zero represents a null v- |
| 	alue (ie. no new input since last check while a 1 |
| 	followed by data represents success.              |
+---------------------------------------------------------+
